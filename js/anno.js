/*
0: 'Jiří Hynek',
1: 'Petr Hannig',
2: 'Pavel Fischer',
3: 'Michal Horáček',
4: 'Vratislav Kulhánek',
5: 'Mirek Topolánek',
6: 'Jiří Drahoš',
7: 'Marek Hilšer',
8: 'Miloš Zeman'
*/

// i před datem značí důležitou anotaci

var anno = {
    0: {
        '08-21': '<a href="https://www.irozhlas.cz/zpravy-domov/realiste-predstavili-prezidentskeho-kandidata-je-jim-sef-asociace-zbrojaru-jiri_1708211026_jgr">Hynek oznámil kandidaturu</a>',
        '10-17': '<a href="https://www.irozhlas.cz/zpravy-domov/dalsi-prezidentsky-kandidat-jiriho-hynka-podporilo-pres-20-poslancu_1710171040_ako">Hynek podal kandidaturu</a>',
    },
    1: {
        '07-19': '<a href="https://www.irozhlas.cz/zpravy-domov/ctrnacty-uchazec-v-boji-o-hrad-kandidaturu-oznamil-hudebnik-petr-hannig_1707191657_jra">Hannig oznámil kandidaturu</a>',
        '10-26': '<a href="https://www.irozhlas.cz/zpravy-domov/hannig-50-tisic-podpisu-nesehnal-kandidovat-na-prezidenta-ale-bude-podporilo-ho_1710261215_haf">získal 20 poslanců</a>'
    },
    2: {
        '10-04': '<a href="https://www.irozhlas.cz/zpravy-domov/byvaly-diplomat-pavel-fischer-chce-kandidovat-na-prezidenta-podporu-nasel-mezi_1710041306_kno">Fischer oznámil kandidaturu</a>'
    },
    3: {
        '02-09': '<a href="https://www.irozhlas.cz/zpravy-domov/kandidat-na-prezidenta-horacek-predstavil-poradce-pomahat-mu-bude-vasaryova-reichel-i-drabova_201702091355_pjadrny">Horáček představil poradce</a>',
        '03-10': '<a href="https://www.irozhlas.cz/zpravy-domov/nepovedu-osobni-kampan-nepujdu-do-debat-s-protikandidaty-popsal-zeman-prezidentskou-kandidaturu_201703101150_lreznik">Zeman potvrdil kandidaturu</a>',
        '04-16': '<a href="https://www.irozhlas.cz/zpravy-z-domova/zemana-volilo-37-procent-lidi-horacek-a-drahos-svedli-boj-o-postupove-misto_1704170724_">průzkum Medianu</a>'
    },
    4: {
        '06-29': '<a href="https://www.irozhlas.cz/zpravy-domov/novy-prezidentsky-kandidat-byvalemu-sefovi-skody-auto-kulhankovi-ma-pomoci_1706291055_pj">Kulhánek oznámi kandidaturu</a>',
        '10-18': '<a href="https://www.irozhlas.cz/zpravy-domov/kulhanek-oznamil-ministerstvu-prezidentskou-kandidaturu-byvaly-sef-skody-auto_1710181014_pj">Kulhánek získal podpisy 23 poslanců</a>'
    },
    5: {
        '10-31': '<a href="http://www.ceskatelevize.cz/ivysilani/11412378947-90-ct24/217411058131031/obsah/578547-vlady-mirka-topolanka-hyde-park">rozhovor pro Českou televizi</a>',
        '11-06': '<a href="https://www.irozhlas.cz/zpravy-domov/topolanek-sehnal-podpisy-pro-kandidaturu-na-hrad-byl-jsem-posledni-tvrdi-senator_1711061031_hm">Topolánek sehnal podpisy senátorů</a>'
    },
    6: {
        '03-29': '<a href="https://www.irozhlas.cz/zpravy-domov/kandidaturu-drahose-politici-privitali-ovcacek-ho-nazval-fikusem-_201703282100_ph">Drahoš oznámil kandidaturu, Ovčáček ho nazval fíkusem</a>',
        '02-19': '<a href="http://www.ceskatelevize.cz/ivysilani/874586-gen/216562261300011-jiri-drahos">Drahoš má narozeniny, ČT vysílá jeho portrét</a>',
        '11-07': '<a href="https://www.irozhlas.cz/volby/tesny-souboj-ve-druhem-kole-drahos-ma-podle-pruzkumu-pred-zemanem-nepatrny_1711071540_ako">uzávěrka kandidátek</a>'
    },
    7: {'03-10': '<a href="https://www.irozhlas.cz/zpravy-domov/nepovedu-osobni-kampan-nepujdu-do-debat-s-protikandidaty-popsal-zeman-prezidentskou-kandidaturu_201703101150_lreznik">Zeman potvrdil kandidaturu</a>',
        '03-20': '<a href="https://www.seznam.cz/zpravy/clanek/milos-zeman-je-srab-a-boji-se-skladat-ucty-prezidentsky-kandidat-hilser-priostril-29158">rozhovor pro Seznam</a>',
        '11-02': '<a href="https://www.irozhlas.cz/zpravy-domov/hilser-se-chysta-na-vnitru-zaregistrovat-kandidaturu-na-hrad-podporu-nakonec_1711021009_kno">Hilšer podal kandidaturu</a>'
    },
    8: {
        '03-10': '<a href="https://www.irozhlas.cz/zpravy-domov/nepovedu-osobni-kampan-nepujdu-do-debat-s-protikandidaty-popsal-zeman-prezidentskou-kandidaturu_201703101150_lreznik">Zeman potvrdil kandidaturu</a>',
        '05-05': '<a href="https://www.irozhlas.cz/zpravy-domov/prave-vysilame-sobotka-oznami-na-cem-se-rano-dohodli-spicky-cssd_1705051015_jra">kritika od senátorů</a>',
        '05-17': '<a href="https://www.irozhlas.cz/zpravy-domov/na-druhou-demonstraci-proti-babisovi-prislo-na-vaclavske-namesti-pet-tisic-lidi_1705181118_pek">demonstrace</a>',
        '10-28': '<a href="https://www.irozhlas.cz/zpravy-domov/online-prezident-rozda-vecer-statni-vyznamenani-oceneni-prevezmou-sablikova-i_1710281930_ako">státní vyznamenání</a>'
    }
};