var cands = {
    0: 'Jiří Hynek',
    1: 'Petr Hannig',
    2: 'Pavel Fischer',
    3: 'Michal Horáček',
    4: 'Vratislav Kulhánek',
    5: 'Mirek Topolánek',
    6: 'Jiří Drahoš',
    7: 'Marek Hilšer',
    8: 'Miloš Zeman'
};

//https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
function thousandSep(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

function drawChart(candId) {
    var chartData = [];
    for (var day in days) {
        chartData.push([day, days[day][candId]])
    };

    var lbls = [];
    var lbls_imp = [];
    for (var lbl in anno[candId]) {
        if (lbl.startsWith('i')) {
            lbl = lbl.replace('i', '')
            lbls_imp.push({
                point: {
                    xAxis: 0,
                    yAxis: 0,
                    x: Object.keys(days).indexOf(lbl),
                    y: days[lbl][candId]
                },
                text: anno[candId]['i' + lbl]
            });
        } else {
            lbls.push({
                point: {
                    xAxis: 0,
                    yAxis: 0,
                    x: Object.keys(days).indexOf(lbl),
                    y: days[lbl][candId]
                },
                text: anno[candId][lbl]
            });
        }
    };

    Highcharts.chart('kand_' + candId, {
        chart: {
            type: 'line',
            panning: false
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Návštěvy hesla <a target="_blank" href="https://cs.wikipedia.org/wiki/' + cands[candId] + '">' + cands[candId] + '</a>',
            useHTML: true
        },
        subtitle: {
            text: 'na Wikipedii k 19. listopadu 2017'
        },
        annotations: [{
            labelOptions: {
                shape: 'connector',
                align: 'right',
                justify: false,
                crop: true,
                style: {
                    fontSize: '0.8em',
                    textOutline: '1px white'
                }
            },
            labels: lbls
        }, {
            labelOptions: {
                backgroundColor: 'rgba(255,255,255,0.5)',
                verticalAlign: 'top',
                y: 15
            },
            labels: lbls_imp
        }],
        xAxis: {
            labels: {
                formatter: function() {
                    if (this.value in chartData) {
                        var val = chartData[this.value][0].split('-')
                        return parseInt(val[1]) + '. ' + parseInt(val[0]) + '. 2017';
                    } else {
                        return '';
                    }
                    
                }
            },
            minRange: 5,
            title: {
                text: 'den'
            }
        },
        yAxis: {
            startOnTick: true,
            endOnTick: false,
            maxPadding: 0.35,
            max: 12000,
            title: {
                text: null
            },
            labels: {
                format: '{value:,.0f}'
            },
            title: {
                text: 'zobrazení'
            }
        },
        tooltip: {
            formatter: function() {
                var val = this.points[0].key.split('-');
                return '<b>' + parseInt(val[1]) + '. ' + parseInt(val[0]) + '. 2017</b><br>' + thousandSep(this.y) + ' zobrazení';
            },
            shared: true
        },
        legend: {
            enabled: false
        },
        series: [{
            data: chartData,
            color: Highcharts.getOptions().colors[candId],
            fillOpacity: 0.5,
            name: 'zobrazeni',
            marker: {
                enabled: false
            },
            threshold: null
        }]
    });
};

$('.prezident').each(function(i) {
    drawChart(parseInt($(this).prop('id').split('_')[1]))
});